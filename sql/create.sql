CREATE DATABASE  IF NOT EXISTS `audiofeel` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `audiofeel`;
-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: audiofeel
-- ------------------------------------------------------
-- Server version	5.7.20-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `albums`
--

DROP TABLE IF EXISTS `albums`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `albums` (
  `album_id` int(11) NOT NULL AUTO_INCREMENT,
  `album_title` varchar(45) NOT NULL,
  `release_date` date DEFAULT NULL,
  `cover` blob,
  PRIMARY KEY (`album_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `albums`
--

LOCK TABLES `albums` WRITE;
/*!40000 ALTER TABLE `albums` DISABLE KEYS */;
/*!40000 ALTER TABLE `albums` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `artists`
--

DROP TABLE IF EXISTS `artists`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `artists` (
  `artist_id` int(11) NOT NULL AUTO_INCREMENT,
  `artist_name` varchar(45) NOT NULL,
  PRIMARY KEY (`artist_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `artists`
--

LOCK TABLES `artists` WRITE;
/*!40000 ALTER TABLE `artists` DISABLE KEYS */;
/*!40000 ALTER TABLE `artists` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `artists_join_albums`
--

DROP TABLE IF EXISTS `artists_join_albums`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `artists_join_albums` (
  `artist_id` int(11) NOT NULL,
  `album_id` int(11) NOT NULL,
  KEY `artist_id` (`artist_id`),
  KEY `album_id` (`album_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `artists_join_albums`
--

LOCK TABLES `artists_join_albums` WRITE;
/*!40000 ALTER TABLE `artists_join_albums` DISABLE KEYS */;
/*!40000 ALTER TABLE `artists_join_albums` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `artists_join_genres`
--

DROP TABLE IF EXISTS `artists_join_genres`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `artists_join_genres` (
  `artist_id` int(11) NOT NULL,
  `genre_id` int(11) NOT NULL,
  KEY `artist_id` (`artist_id`),
  KEY `genre_id` (`genre_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `artists_join_genres`
--

LOCK TABLES `artists_join_genres` WRITE;
/*!40000 ALTER TABLE `artists_join_genres` DISABLE KEYS */;
/*!40000 ALTER TABLE `artists_join_genres` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `attribute_join_similarity_computation_method`
--

DROP TABLE IF EXISTS `attribute_join_similarity_computation_method`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `attribute_join_similarity_computation_method` (
  `attribute_join_similarity_computation_method_id` int(11) NOT NULL AUTO_INCREMENT,
  `method_id` int(11) NOT NULL,
  `attribute_id` int(11) NOT NULL,
  `max` double DEFAULT NULL,
  `min` double DEFAULT NULL,
  PRIMARY KEY (`attribute_join_similarity_computation_method_id`),
  UNIQUE KEY `attribute_join_similarity_computation_method_id_UNIQUE` (`attribute_join_similarity_computation_method_id`)
) ENGINE=InnoDB AUTO_INCREMENT=351 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `attribute_join_similarity_computation_method`
--

LOCK TABLES `attribute_join_similarity_computation_method` WRITE;
/*!40000 ALTER TABLE `attribute_join_similarity_computation_method` DISABLE KEYS */;
/*!40000 ALTER TABLE `attribute_join_similarity_computation_method` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `attributes`
--

DROP TABLE IF EXISTS `attributes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `attributes` (
  `attribute_id` int(11) NOT NULL AUTO_INCREMENT,
  `attribute_name` varchar(255) NOT NULL,
  `attribute_description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`attribute_id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `attributes`
--

LOCK TABLES `attributes` WRITE;
/*!40000 ALTER TABLE `attributes` DISABLE KEYS */;
/*!40000 ALTER TABLE `attributes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `attributes_join_tracks`
--

DROP TABLE IF EXISTS `attributes_join_tracks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `attributes_join_tracks` (
  `attribute_id` int(11) NOT NULL,
  `track_id` int(11) NOT NULL,
  `value_number` int(11) NOT NULL,
  `value` double NOT NULL,
  `attributes_join_tracks_id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`attributes_join_tracks_id`)
) ENGINE=InnoDB AUTO_INCREMENT=16625 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `attributes_join_tracks`
--

LOCK TABLES `attributes_join_tracks` WRITE;
/*!40000 ALTER TABLE `attributes_join_tracks` DISABLE KEYS */;
/*!40000 ALTER TABLE `attributes_join_tracks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `featurings`
--

DROP TABLE IF EXISTS `featurings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `featurings` (
  `track_id` int(11) NOT NULL,
  `artist_id` int(11) NOT NULL,
  KEY `track_id` (`track_id`),
  KEY `artist_id` (`artist_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `featurings`
--

LOCK TABLES `featurings` WRITE;
/*!40000 ALTER TABLE `featurings` DISABLE KEYS */;
/*!40000 ALTER TABLE `featurings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `genres`
--

DROP TABLE IF EXISTS `genres`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `genres` (
  `genre_id` int(11) NOT NULL AUTO_INCREMENT,
  `genre_name` varchar(45) NOT NULL,
  PRIMARY KEY (`genre_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `genres`
--

LOCK TABLES `genres` WRITE;
/*!40000 ALTER TABLE `genres` DISABLE KEYS */;
/*!40000 ALTER TABLE `genres` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `similarity_computation_methods`
--

DROP TABLE IF EXISTS `similarity_computation_methods`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `similarity_computation_methods` (
  `similarity_computation_method_id` int(11) NOT NULL AUTO_INCREMENT,
  `method_name` varchar(45) DEFAULT NULL,
  `method_description` varchar(255) DEFAULT NULL,
  `method_type` varchar(45) NOT NULL,
  PRIMARY KEY (`similarity_computation_method_id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `similarity_computation_methods`
--

LOCK TABLES `similarity_computation_methods` WRITE;
/*!40000 ALTER TABLE `similarity_computation_methods` DISABLE KEYS */;
INSERT INTO `similarity_computation_methods` VALUES (17,'Manhattan Method','Similarities is sum of absolute of subtractions two features with the same index.\nd(x,y) = Ei->n(a,b) abs(ai - bi)','manhattan'),(18,'Modified Manhattan Method','Similarities is sum of absolute of subtractions two features with the same index divided by max of this two features values.\nd(x,y) = Ei->n(a,b) abs(ai - bi) / abs(max(ai, bi))','modified_manhattan'),(19,'Euclidean Method','Similarities is square root from square power of sum of subtractions two features with the same index.\nd(x,y) = sqrt(Ei->n(pow(yi-xi)))','euclidean'),(20,'Jaccard Method','d(x,y) = E(min(xi,yi))/E(max(xi,yi))','jaccard'),(21,'Chebyshev Method','Max differences of features with same index.\nd(x,y) = max(|xi - yi|) ','chebyshev'),(22,'Nearest Neighbour Method','Minimal distance','nn_brute_force'),(23,'Complete Metric Method','d(x,y) = |log(y-x)|','complete_metric');
/*!40000 ALTER TABLE `similarity_computation_methods` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `similarity_informations`
--

DROP TABLE IF EXISTS `similarity_informations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `similarity_informations` (
  `similarity_information_id` int(11) NOT NULL AUTO_INCREMENT,
  `track1_id` int(11) NOT NULL,
  `track2_id` int(11) NOT NULL,
  `method_id` int(11) NOT NULL,
  PRIMARY KEY (`similarity_information_id`)
) ENGINE=InnoDB AUTO_INCREMENT=141 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `similarity_informations`
--

LOCK TABLES `similarity_informations` WRITE;
/*!40000 ALTER TABLE `similarity_informations` DISABLE KEYS */;
/*!40000 ALTER TABLE `similarity_informations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `similarity_informations_join_attributes`
--

DROP TABLE IF EXISTS `similarity_informations_join_attributes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `similarity_informations_join_attributes` (
  `similarity_informations_join_attributes_id` int(11) NOT NULL AUTO_INCREMENT,
  `attribute_id` int(11) NOT NULL,
  `similarity_information_id` int(11) NOT NULL,
  `similarity` double DEFAULT NULL,
  PRIMARY KEY (`similarity_informations_join_attributes_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1543 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `similarity_informations_join_attributes`
--

LOCK TABLES `similarity_informations_join_attributes` WRITE;
/*!40000 ALTER TABLE `similarity_informations_join_attributes` DISABLE KEYS */;
/*!40000 ALTER TABLE `similarity_informations_join_attributes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tracks`
--

DROP TABLE IF EXISTS `tracks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tracks` (
  `track_id` int(11) NOT NULL AUTO_INCREMENT,
  `track_title` varchar(45) NOT NULL,
  `duration` time DEFAULT NULL,
  `album_id` int(11) DEFAULT NULL,
  `track_number` int(11) DEFAULT NULL,
  PRIMARY KEY (`track_id`),
  KEY `album_id` (`album_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tracks`
--

LOCK TABLES `tracks` WRITE;
/*!40000 ALTER TABLE `tracks` DISABLE KEYS */;
/*!40000 ALTER TABLE `tracks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tracks_join_genres`
--

DROP TABLE IF EXISTS `tracks_join_genres`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tracks_join_genres` (
  `track_id` int(11) NOT NULL,
  `genre_id` int(11) NOT NULL,
  KEY `track_id` (`track_id`),
  KEY `genre_id` (`genre_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tracks_join_genres`
--

LOCK TABLES `tracks_join_genres` WRITE;
/*!40000 ALTER TABLE `tracks_join_genres` DISABLE KEYS */;
/*!40000 ALTER TABLE `tracks_join_genres` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-12-11 18:48:04
