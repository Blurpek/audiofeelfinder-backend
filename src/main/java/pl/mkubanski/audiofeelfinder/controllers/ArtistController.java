package pl.mkubanski.audiofeelfinder.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.mkubanski.audiofeelfinder.commands.ArtistCreateCommand;
import pl.mkubanski.audiofeelfinder.commands.IdCommand;
import pl.mkubanski.audiofeelfinder.domains.Artist;
import pl.mkubanski.audiofeelfinder.services.ArtistService;

import java.util.List;

@RestController
@RequestMapping("/artist")
public class ArtistController {
    @Autowired
    ArtistService artistService;

    @PostMapping("/list")
    public List<Artist> list() {
        return artistService.list();
    }

    @PostMapping("/findOne")
    public Artist findOne(@RequestBody IdCommand command) {
        return artistService.findOne(command.getId());
    }

    @PostMapping("/create")
    public Artist create(@RequestBody ArtistCreateCommand command) {
        return artistService.create(command.getName());
    }
}
