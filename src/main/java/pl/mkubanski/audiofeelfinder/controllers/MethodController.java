package pl.mkubanski.audiofeelfinder.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.mkubanski.audiofeelfinder.domains.SimilarityComputationMethod;
import pl.mkubanski.audiofeelfinder.services.MethodService;

import java.util.List;

@RestController
@RequestMapping("/method")
public class MethodController {
    @Autowired
    MethodService methodService;

    @PostMapping
    @RequestMapping("/list")
    public List<SimilarityComputationMethod> list() {
        return methodService.list();
    }
}
