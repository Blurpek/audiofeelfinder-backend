package pl.mkubanski.audiofeelfinder.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import pl.mkubanski.audiofeelfinder.commands.IdCommand;
import pl.mkubanski.audiofeelfinder.commands.TrackTitleArtistAlbumCommand;
import pl.mkubanski.audiofeelfinder.domains.Track;
import pl.mkubanski.audiofeelfinder.services.TrackService;

import javax.validation.Valid;
import javax.validation.ValidationException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/track")
public class TrackController {
    @Autowired
    TrackService trackService;

    @PostMapping("/create")
    public Track create(@Valid @RequestBody Track track) {
        return trackService.create(track);
    }

    @PostMapping("/list")
    public ArrayList<Track> list() {
        return trackService.findAll();
    }

    @PostMapping("/findOne")
    public Track findOne(@RequestBody IdCommand command) {
        return trackService.findOne(command.getId());
    }

    @PostMapping("/findAllByTitleAndArtistAndAlbum")
    public List<Track> findTrackByNameAndArtistAndAlbum(@RequestBody TrackTitleArtistAlbumCommand cmd) {
        if (cmd.getTitle() == null && cmd.getArtist() == null && cmd.getAlbum() == null) {
            return null;
        }
        return trackService.findAllByTitleAndArtistAndAlbum(cmd.getTitle(), cmd.getArtist(), cmd.getAlbum());
    }

    @PostMapping("/add")
    public Track add(
            @RequestParam("file") MultipartFile file,
            @RequestParam("title") String title,
            @RequestParam("albumId") Integer albumId) {
        if (file == null || title == null || title == "" || albumId == null) {
            throw new ValidationException("Invalid params");
        }
        Track track = trackService.createWithFile(file, title, albumId);
        return track;
    }

    @PostMapping("/delete")
    public String delete(Integer id) {
        trackService.delete(id);
        return "success";
    }
}
