package pl.mkubanski.audiofeelfinder.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import pl.mkubanski.audiofeelfinder.domains.Attribute;
import pl.mkubanski.audiofeelfinder.services.AttributeService;

import java.util.List;

@RestController
@RequestMapping("/attribute")
public class AttributeController {
    @Autowired
    AttributeService attributeService;

    @PostMapping
    @RequestMapping("/list")
    public List<Attribute> list() {
        return attributeService.list();
    }
}
