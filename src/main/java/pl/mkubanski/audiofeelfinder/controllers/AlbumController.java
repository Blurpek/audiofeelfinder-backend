package pl.mkubanski.audiofeelfinder.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.mkubanski.audiofeelfinder.commands.AlbumCreateCommand;
import pl.mkubanski.audiofeelfinder.commands.IdCommand;
import pl.mkubanski.audiofeelfinder.domains.Album;
import pl.mkubanski.audiofeelfinder.services.AlbumService;

import javax.validation.ValidationException;
import java.util.Calendar;
import java.util.List;

@RestController
@RequestMapping("/album")
public class AlbumController {
    @Autowired
    AlbumService albumService;

    @PostMapping("/create")
    public Album create(@RequestBody AlbumCreateCommand command) {
        Calendar releaseDate = Calendar.getInstance();
        releaseDate.set(command.getYear(), 1,1);
        return albumService.createWithArtist(command.getTitle(), releaseDate.getTime(), command.getArtistId());

    }

    @PostMapping("/list")
    public List<Album> list() {
        return albumService.findAll();
    }

    @PostMapping("/findOne")
    public Album findOne(@RequestBody IdCommand idCommand) {
        return albumService.findOne(idCommand.getId());
    }

    @PostMapping("/listByArtist")
    public List<Album> listByArtist(@RequestBody IdCommand idCommand) {
        if (idCommand.getId() == null) {
            throw new ValidationException("Id cannot be null");
        }
        return albumService.listByArtist(idCommand.getId());
    }

    @PostMapping("/delete")
    public String delete(@RequestBody IdCommand idCommand) {
        albumService.delete(idCommand.getId());
        return "success";
    }
}
