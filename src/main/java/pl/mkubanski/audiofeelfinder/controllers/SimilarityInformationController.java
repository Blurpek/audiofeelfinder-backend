package pl.mkubanski.audiofeelfinder.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.mkubanski.audiofeelfinder.commands.FindSimilarityCommand;
import pl.mkubanski.audiofeelfinder.domains.SimilarityComputationMethod;
import pl.mkubanski.audiofeelfinder.domains.SimilarityInformation;
import pl.mkubanski.audiofeelfinder.domains.SimilarityInformationsJoinAttributes;
import pl.mkubanski.audiofeelfinder.domains.Track;
import pl.mkubanski.audiofeelfinder.repositories.SimilarityComputationMethodRepository;
import pl.mkubanski.audiofeelfinder.repositories.TrackRepository;
import pl.mkubanski.audiofeelfinder.services.SimilarityInformationService;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.NavigableMap;

@RestController
@RequestMapping("/similarityInformation")
public class SimilarityInformationController {
    @Autowired
    SimilarityInformationService similarityInformationService;

    @Autowired
    TrackRepository trackRepository;

    @Autowired
    SimilarityComputationMethodRepository methodRepository;

    @PostMapping
    @RequestMapping("/findSimilar")
    public NavigableMap<Double, SimilarityInformation> findSimilar(@RequestBody FindSimilarityCommand command) {
        NavigableMap<Double, SimilarityInformation> similarityInformations = similarityInformationService.findSimilar(command);
        return similarityInformations;
    }
}
