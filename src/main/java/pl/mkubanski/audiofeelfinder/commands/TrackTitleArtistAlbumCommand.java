package pl.mkubanski.audiofeelfinder.commands;

import lombok.Getter;

public class TrackTitleArtistAlbumCommand {
    @Getter
    String title;
    @Getter
    String artist;
    @Getter
    String album;
}
