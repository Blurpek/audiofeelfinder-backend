package pl.mkubanski.audiofeelfinder.commands;

import lombok.Getter;

public class IdCommand {
    @Getter
    Integer id;
}
