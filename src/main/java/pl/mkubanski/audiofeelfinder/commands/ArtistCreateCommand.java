package pl.mkubanski.audiofeelfinder.commands;

import lombok.Getter;

public class ArtistCreateCommand {
    @Getter
    private String name;
}
