package pl.mkubanski.audiofeelfinder.commands;

import lombok.Getter;

public class FindSimilarityCommand {
    @Getter
    Integer trackId;
    @Getter
    Integer methodId;
    @Getter
    Integer numberOfNeighbours;
}
