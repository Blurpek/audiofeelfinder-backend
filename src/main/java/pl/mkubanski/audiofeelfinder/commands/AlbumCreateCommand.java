package pl.mkubanski.audiofeelfinder.commands;

import lombok.Getter;

public class AlbumCreateCommand {
    @Getter
    private String title;
    @Getter
    private Integer year;
    @Getter
    private Integer artistId;
}
