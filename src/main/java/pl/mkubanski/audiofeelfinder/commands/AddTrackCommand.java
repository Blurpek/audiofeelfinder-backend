package pl.mkubanski.audiofeelfinder.commands;

import lombok.Getter;
import org.springframework.web.multipart.MultipartFile;

public class AddTrackCommand {
    @Getter
    MultipartFile file;
}
