package pl.mkubanski.audiofeelfinder.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.mkubanski.audiofeelfinder.domains.SimilarityComputationMethod;
import pl.mkubanski.audiofeelfinder.domains.methods.*;
import pl.mkubanski.audiofeelfinder.repositories.SimilarityComputationMethodRepository;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class MethodService {
    @Autowired
    SimilarityComputationMethodRepository similarityComputationMethodRepository;

    public List<SimilarityComputationMethod> list() {
        return similarityComputationMethodRepository.findAll();
    }

    public void addAllMethods() {
        addManhattanMethod();
        addModifiedManhattanMethod();
        addEuclideanMethod();
        addJaccardMethod();
        addChebyshevMethod();
        addNNBruteForceMethod();
        addCompleteMetricMethod();
    }

    public void addManhattanMethod() {
        similarityComputationMethodRepository.save(new ManhattanMethod(
                "Manhattan Method",
                "Similarities is sum of absolute of subtractions two features with the same index.\nd(x,y) = Ei->n(a,b) abs(ai - bi)"
        ));
    }

    public void addModifiedManhattanMethod() {
        similarityComputationMethodRepository.save(new ModifiedManhattanMethod(
                "Modified Manhattan Method",
                "Similarities is sum of absolute of subtractions two features with the same index divided by max of this two features values.\nd(x,y) = Ei->n(a,b) abs(ai - bi) / abs(max(ai, bi))"
        ));
    }

    public void addEuclideanMethod() {
        similarityComputationMethodRepository.save(new EuclideanMethod(
                "Euclidean Method",
                "Similarities is square root from square power of sum of subtractions two features with the same index.\nd(x,y) = sqrt(Ei->n(pow(yi-xi)))"
        ));
    }

    public void addJaccardMethod() {
        similarityComputationMethodRepository.save(new MinDivMaxMethod(
                "Jaccard Method",
                "d(x,y) = E(min(xi,yi))/E(max(xi,yi))"
        ));
    }

    public void addChebyshevMethod() {
        similarityComputationMethodRepository.save(new ChebyshevMethod(
                "Chebyshev Method",
                "Max differences of features with same index.\nd(x,y) = max(|xi - yi|) "
        ));
    }

    public void addNNBruteForceMethod() {
        similarityComputationMethodRepository.save(new NNBruteForceMethod(
                "Nearest Neighbour Method",
                "Minimal distance"
        ));
    }

    public void addCompleteMetricMethod() {
        similarityComputationMethodRepository.save(new CompleteMetricMethod(
                "Complete Metric Method",
                "d(x,y) = |log(y-x)|"
        ));
    }

}
