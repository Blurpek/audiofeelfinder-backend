package pl.mkubanski.audiofeelfinder.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.mkubanski.audiofeelfinder.commands.FindSimilarityCommand;
import pl.mkubanski.audiofeelfinder.domains.*;
import pl.mkubanski.audiofeelfinder.repositories.*;

import javax.transaction.Transactional;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Transactional
public class SimilarityInformationService {
    @Autowired
    TrackService trackService;

    @Autowired
    AttributeJoinTrackRepository attributeJoinTrackRepository;

    @Autowired
    AttributeService attributeService;

    @Autowired
    SimilarityComputationMethodRepository similarityComputationMethodRepository;

    @Autowired
    SimilarityInformationsJoinAttributesRepository similarityInformationsJoinAttributesRepository;

    @Autowired
    SimilarityInformationRepository similarityInformationRepository;

    @Autowired
    TrackRepository trackRepository;

    @Autowired
    AttributeJoinSimilarityComputationMethodService attributeJoinSimilarityComputationMethodService;

    public NavigableMap<Double, SimilarityInformation> findSimilar(FindSimilarityCommand command) {
        Track track = trackRepository.findOne(command.getTrackId());
        SimilarityComputationMethod method = similarityComputationMethodRepository.findOne(command.getMethodId());
        if (track == null) {
            throw new RuntimeException("SimilarityInformationService.findSimilar - not found track by id");
        }
        if (method == null) {
            throw new RuntimeException("SimilarityInformationService.findSimilar - not found methods by id");
        }
        Integer numberOfNearestNeighbours = command.getNumberOfNeighbours();
        NavigableMap<Double, SimilarityInformation> nearestNeighbours = new TreeMap<>();
        Set<Attribute> trackAttributes = attributeService.getAttributesForTrack(track);
        List<Track> tracksToTest = trackService.findAllWithAttributes(trackAttributes);
        tracksToTest.remove(track);
        Double localDistance = 0d;
        Double minDistance = Double.MAX_VALUE;

        for (int i = 0; i < (tracksToTest.size() < numberOfNearestNeighbours ? tracksToTest.size() : numberOfNearestNeighbours); ++i) {
            SimilarityInformation similarityInformation =
                    similarityInformationRepository.findByTrack1AndTrack2AndMethod(track,tracksToTest.get(i),method);

            if (similarityInformation == null) {
                similarityInformation = create(track, tracksToTest.get(i), method, trackAttributes);
            }
            localDistance = computeSimilarity(similarityInformation);
            nearestNeighbours.put(localDistance, similarityInformation);
        }

        minDistance = nearestNeighbours.lastKey();

        for (int i = numberOfNearestNeighbours; i < tracksToTest.size(); ++i) {
            SimilarityInformation similarityInformation =
                    similarityInformationRepository.findByTrack1AndTrack2AndMethod(track,tracksToTest.get(i),method);

            if (similarityInformation == null) {
                similarityInformation = create(track, tracksToTest.get(i), method, trackAttributes);
            }
            localDistance = computeSimilarity(similarityInformation);
            if (localDistance < minDistance) {
                nearestNeighbours.pollLastEntry();
                nearestNeighbours.put(localDistance, similarityInformation);
            }
        }
        return nearestNeighbours;
    }

    public Double computeSimilarity(SimilarityInformation similarityInformation) {
        Double similarity = 0d;
        for (SimilarityInformationsJoinAttributes sija : similarityInformation.getSimilarities()) {
            similarity += normalize(sija, similarityInformation.getMethod());
        }
        return similarity / similarityInformation.getSimilarities().size();
    }

    private Double checkForInfinityOrNan(Double d) {
        if (d.isNaN()) {
            d = Double.MIN_NORMAL;
        } else if (d.isInfinite()) {
            d = Double.MAX_VALUE;
        }
        return d;
    }

    //normalization method: feature scaling
    private Double normalize(SimilarityInformationsJoinAttributes sija, SimilarityComputationMethod method) {
        AttributeJoinSimilarityComputationMethod attributeJoinSimilarityComputationMethod = attributeJoinSimilarityComputationMethodService.findByMethodAndAttribute(method, sija.getAttribute());
        if (attributeJoinSimilarityComputationMethod == null) {
            attributeJoinSimilarityComputationMethod = attributeJoinSimilarityComputationMethodService.create(sija.getAttribute(), method);
        }
        if (sija.getSimilarity() < attributeJoinSimilarityComputationMethod.getMin()) {
            attributeJoinSimilarityComputationMethod.setMin(sija.getSimilarity());
            attributeJoinSimilarityComputationMethodService.save(attributeJoinSimilarityComputationMethod);
        } else if (sija.getSimilarity() > attributeJoinSimilarityComputationMethod.getMax()) {
            attributeJoinSimilarityComputationMethod.setMax(sija.getSimilarity());
            attributeJoinSimilarityComputationMethodService.save(attributeJoinSimilarityComputationMethod);
        }
        Double divisor = checkForInfinityOrNan((attributeJoinSimilarityComputationMethod.getMax() - attributeJoinSimilarityComputationMethod.getMin()));

        return checkForInfinityOrNan((sija.getSimilarity() - attributeJoinSimilarityComputationMethod.getMin()) / divisor);
    }

    public SimilarityInformation create(Track track, Track trackToTest, SimilarityComputationMethod method, Set<Attribute> trackAttributes) {
        SimilarityInformation similarityInformation = similarityInformationRepository.save(new SimilarityInformation(
                track,
                trackToTest,
                method
        ));
        List<SimilarityInformationsJoinAttributes> similarities = new ArrayList<>();

        for (Attribute a : trackAttributes) {
            List<AttributeJoinTrack> trackAttributeFeatures = attributeJoinTrackRepository.findAllByAttributeAndTrack(a, track);
            List<AttributeJoinTrack> testingTrackAttributeFeatures = attributeJoinTrackRepository.findAllByAttributeAndTrack(a, trackToTest);

            Double similarity = method.compute(trackAttributeFeatures, testingTrackAttributeFeatures);
            similarity = checkForInfinityOrNan(similarity);
            similarityInformation.getSimilarities().add(similarityInformationsJoinAttributesRepository.save(
                    new SimilarityInformationsJoinAttributes(
                            a,
                            similarityInformation,
                            similarity
                    )
            ));
        }
        return similarityInformation;
    }

    public List<SimilarityInformation> findAll() {
        return similarityInformationRepository.findAll();
    }
}
