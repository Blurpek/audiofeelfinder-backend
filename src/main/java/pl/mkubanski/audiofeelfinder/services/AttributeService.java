package pl.mkubanski.audiofeelfinder.services;

import jAudioFeatureExtractor.ACE.DataTypes.Batch;
import org.apache.commons.lang3.SystemUtils;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import pl.mkubanski.audiofeelfinder.domains.Attribute;
import pl.mkubanski.audiofeelfinder.domains.AttributeJoinTrack;
import pl.mkubanski.audiofeelfinder.domains.Track;
import pl.mkubanski.audiofeelfinder.repositories.AttributeJoinTrackRepository;
import pl.mkubanski.audiofeelfinder.repositories.AttributeRepository;
import pl.mkubanski.audiofeelfinder.repositories.TrackRepository;

import javax.sound.sampled.*;
import javax.transaction.Transactional;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

@Service
@Transactional
public class AttributeService {
    @Autowired
    AttributeRepository attributeRepository;

    @Autowired
    AttributeJoinTrackRepository attributeJoinTrackRepository;

    @Autowired
    TrackRepository trackRepository;

    @Autowired
    SimilarityInformationService similarityInformationService;


    public static byte[] getAudioDataBytes(byte[] sourceBytes, AudioFormat audioFormat) throws UnsupportedAudioFileException, IllegalArgumentException, Exception {
        if (sourceBytes == null || sourceBytes.length == 0 || audioFormat == null) {
            throw new IllegalArgumentException("Illegal Argument passed to this methods");
        }

        try (final ByteArrayInputStream bais = new ByteArrayInputStream(sourceBytes);
             final AudioInputStream sourceAIS = AudioSystem.getAudioInputStream(bais)) {
            AudioFormat sourceFormat = sourceAIS.getFormat();
            AudioFormat convertFormat = new AudioFormat(AudioFormat.Encoding.PCM_SIGNED, sourceFormat.getSampleRate(), 16, sourceFormat.getChannels(), sourceFormat.getChannels() * 2, sourceFormat.getSampleRate(), false);
            try (final AudioInputStream convert1AIS = AudioSystem.getAudioInputStream(convertFormat, sourceAIS);
                 final AudioInputStream convert2AIS = AudioSystem.getAudioInputStream(audioFormat, convert1AIS);
                 final ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
                byte[] buffer = new byte[8192];
                while (true) {
                    int readCount = convert2AIS.read(buffer, 0, buffer.length);
                    if (readCount == -1) {
                        break;
                    }
                    baos.write(buffer, 0, readCount);
                }
                return baos.toByteArray();
            }
        }
    }

    public static byte [] getAudioDataBytes2(byte [] sourceBytes, AudioFormat audioFormat) throws UnsupportedAudioFileException, IllegalArgumentException, Exception{
        if(sourceBytes == null || sourceBytes.length == 0 || audioFormat == null){
            throw new IllegalArgumentException("Illegal Argument passed to this methods");
        }

        ByteArrayInputStream bais = null;
        ByteArrayOutputStream baos = null;
        AudioInputStream sourceAIS = null;
        AudioInputStream convert1AIS = null;
        AudioInputStream convert2AIS = null;

        try{
            bais = new ByteArrayInputStream(sourceBytes);
            sourceAIS = AudioSystem.getAudioInputStream(bais);
            AudioFormat sourceFormat = sourceAIS.getFormat();
            AudioFormat convertFormat = new AudioFormat(AudioFormat.Encoding.PCM_SIGNED, sourceFormat.getSampleRate(), 16, sourceFormat.getChannels(), sourceFormat.getChannels()*2, sourceFormat.getSampleRate(), false);
            convert1AIS = AudioSystem.getAudioInputStream(convertFormat, sourceAIS);
            convert2AIS = AudioSystem.getAudioInputStream(audioFormat, convert1AIS);

            baos = new ByteArrayOutputStream();

            byte [] buffer = new byte[8192];
            while(true){
                int readCount = convert2AIS.read(buffer, 0, buffer.length);
                if(readCount == -1){
                    break;
                }
                baos.write(buffer, 0, readCount);
            }
            return baos.toByteArray();
        } catch(UnsupportedAudioFileException uafe){
            //uafe.printStackTrace();
            throw uafe;
        } catch(IOException ioe){
            //ioe.printStackTrace();
            throw ioe;
        } catch(IllegalArgumentException iae){
            //iae.printStackTrace();
            throw iae;
        } catch (Exception e) {
            //e.printStackTrace();
            throw e;
        }finally{
            if(baos != null){
                try{
                    baos.close();
                }catch(Exception e){
                }
            }
            if(convert2AIS != null){
                try{
                    convert2AIS.close();
                }catch(Exception e){
                }
            }
            if(convert1AIS != null){
                try{
                    convert1AIS.close();
                }catch(Exception e){
                }
            }
            if(sourceAIS != null){
                try{
                    sourceAIS.close();
                }catch(Exception e){
                }
            }
            if(bais != null){
                try{
                    bais.close();
                }catch(Exception e){
                }
            }
        }
    }

    public File convertToFile(MultipartFile multipartFile) throws IOException {
        String fileName = multipartFile.getOriginalFilename();
        byte[] audio = null;
        audio = multipartFile.getBytes();
        File file = new File(fileName);
        file.createNewFile();
        FileOutputStream fos = new FileOutputStream(file);
        fos.write(audio);
        fos.close();
        if (fileName.toLowerCase().endsWith(".mp3")) {
            String newFileName = fileName.substring(0, fileName.lastIndexOf(".mp3")) + ".wav";
            Process process = null;
            if (SystemUtils.IS_OS_WINDOWS) {
                process = new ProcessBuilder(System.getenv("FFMPEG_HOME") + "\\bin\\ffmpeg", "-i", "\"" + fileName + "\"", "\"" + newFileName + "\"").start();
            } else {
                process = new ProcessBuilder("ffmpeg", "-i", fileName, newFileName).start();
            }

            try {
                Integer val = process.waitFor();
                Files.deleteIfExists(Paths.get(file.getPath()));
                file = new File(newFileName);
                return file;
            } catch (InterruptedException e) {
                throw new RuntimeException("File Interrupted");
            }
        } else if (fileName.toLowerCase().endsWith(".wav")) {
            return file;
        } else {
            throw new RuntimeException("Wrong input format. Works only .mp3 and .wav");
        }
    }

    public void generateAttributesForTrack(MultipartFile multipartFile, Integer trackId) {
        Track track = trackRepository.findOne(trackId);
        if (track == null || attributeJoinTrackRepository.existsByTrack(track)) {
            return;
        }
        File featureKeys = new File("destFK.xml");
        File featureValues = new File("destFV.xml");
        fetchFeaturesFromAudioAndSaveToFiles(multipartFile, featureKeys, featureValues);
        fetchFeaturesFromFilesAndSave(featureValues, featureKeys, track);
    }

    private void fetchFeaturesFromFilesAndSave(File featureValues, File featureKeys, Track track) {
        SAXBuilder saxBuilder = new SAXBuilder();
        try {
            Document document = saxBuilder.build(featureValues);
            Element rootElement = document.getRootElement();
            List<Element> features = rootElement.getChildren();
            for (Element feature : features.get(1).getChildren("feature")) {
                String featureName = feature.getChildText("name");
                pl.mkubanski.audiofeelfinder.domains.Attribute attribute = attributeRepository.findByName(featureName);
                if (attribute == null) {
                    String description = fetchFeatureDescription(featureKeys, featureName);
                    attribute = createAttribute(featureName, description);
                }
                int value_number = 0;
                for (Element ee : feature.getChildren("v")) {
                    Double v = Double.parseDouble(ee.getText());
                    AttributeJoinTrack ajt = new AttributeJoinTrack(attribute, track, value_number, v);
                    attributeJoinTrackRepository.save(ajt);
                    value_number++;
                }
            }
        } catch (JDOMException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String fetchFeatureDescription(File featureKeys, String featureName) {
        SAXBuilder saxBuilder = new SAXBuilder();
        try {
            Document document = saxBuilder.build(featureKeys);
            Element rootElement = document.getRootElement();
            List<Element> features = rootElement.getChildren("feature");
            for (Element feature : features) {
                String name = feature.getChildText("name");
                if (name.equalsIgnoreCase(featureName)) {
                    return feature.getChildText("description");
                }
            }
        } catch (JDOMException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }

    private pl.mkubanski.audiofeelfinder.domains.Attribute createAttribute(String featureName, String description) {
        pl.mkubanski.audiofeelfinder.domains.Attribute attribute = new pl.mkubanski.audiofeelfinder.domains.Attribute(featureName, description);
        return attributeRepository.save(attribute);
    }

    //TODO: Make it as thread
    public void fetchFeaturesFromAudioAndSaveToFiles(MultipartFile multipartFile, File featureKeys, File featureValues) {
        try {
            File audioFile = convertToFile(multipartFile);
            File[] files = new File[1];
            files[0] = audioFile;
            Batch batch = new Batch();
            batch.setRecordings(files);
            batch.setSettings(new File("settings.xml").getAbsolutePath());
            batch.getDataModel().featureKey = new FileOutputStream(featureKeys.getAbsolutePath());
            batch.getDataModel().featureValue = new FileOutputStream(featureValues.getAbsolutePath());
            batch.execute();
            Files.deleteIfExists(Paths.get(audioFile.getPath()));
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public Set<Attribute> getAttributesForTrack(Track track) {
        List<AttributeJoinTrack> trackFeatures = attributeJoinTrackRepository.findAllByTrack(track);
        Set<Attribute> trackAttributes = new HashSet<>();
        for (AttributeJoinTrack ajt : trackFeatures) {
            trackAttributes.add(ajt.getAttribute());
        }
        return trackAttributes;
    }

    public List<Attribute> list() {
        return attributeRepository.findAll();
    }

    public Attribute save(Attribute a) {
        return attributeRepository.save(a);
    }
}
