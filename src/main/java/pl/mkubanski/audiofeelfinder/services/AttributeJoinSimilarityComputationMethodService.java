package pl.mkubanski.audiofeelfinder.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.mkubanski.audiofeelfinder.domains.Attribute;
import pl.mkubanski.audiofeelfinder.domains.AttributeJoinSimilarityComputationMethod;
import pl.mkubanski.audiofeelfinder.domains.SimilarityComputationMethod;
import pl.mkubanski.audiofeelfinder.repositories.AttributeJoinSimilarityComputationMethodRepository;

import javax.transaction.Transactional;

@Service
@Transactional
public class AttributeJoinSimilarityComputationMethodService {
    @Autowired
    AttributeJoinSimilarityComputationMethodRepository repository;

    public AttributeJoinSimilarityComputationMethod findByMethodAndAttribute(SimilarityComputationMethod method, Attribute attribute) {
        return repository.findByMethodAndAndAttribute(method, attribute);
    }

    public AttributeJoinSimilarityComputationMethod save(AttributeJoinSimilarityComputationMethod attributeJoinSimilarityComputationMethod) {
        return repository.save(attributeJoinSimilarityComputationMethod);
    }

    public AttributeJoinSimilarityComputationMethod create(Attribute attribute, SimilarityComputationMethod method) {
        return repository.save(new AttributeJoinSimilarityComputationMethod(
                attribute,
                method,
                Double.MIN_VALUE,
                Double.MAX_VALUE
        ));
    }
}
