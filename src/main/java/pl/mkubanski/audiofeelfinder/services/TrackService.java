package pl.mkubanski.audiofeelfinder.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import pl.mkubanski.audiofeelfinder.domains.*;
import pl.mkubanski.audiofeelfinder.repositories.AttributeJoinTrackRepository;
import pl.mkubanski.audiofeelfinder.repositories.TrackRepository;

import javax.transaction.Transactional;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Transactional
public class TrackService {
    @Autowired
    TrackRepository trackRepository;

    @Autowired
    AttributeJoinTrackRepository attributeJoinTrackRepository;

    @Autowired
    AttributeService attributeService;

    @Autowired
    ArtistService artistService;

    @Autowired
    AlbumService albumService;

    public Track create(Track track) {
        return trackRepository.save(track);
    }

    public ArrayList<Track> findAll() {
        return (ArrayList<Track>) trackRepository.findAll();
    }

    public Track findOne(Integer id) {
        return trackRepository.findOne(id);
    }

    public void delete(Integer id) {
        trackRepository.delete(id);
    }

    public List<Track> findAllWithAttributes(Set<Attribute> attributes) {
        List<Track> tracks = new ArrayList<>();
        for (Track track : trackRepository.findAll()) {
            Set<Attribute> trackAttributes = attributeService.getAttributesForTrack(track);
            if (trackAttributes.equals(attributes)) {
                tracks.add(track);
            }
        }
        return tracks;
    }

    public List<Track> findAllByTitleAndArtistAndAlbum(String title, String artist, String album) {
        List<Track> tracks = findAllByTitleContaining(title);
        List<Track> resultTracks = new ArrayList<>();
        Set<Track> setResultTracks = new TreeSet<>();
        List<Album> albumsFromArtists = albumService.findAllByArtistsContaining(artist);
        List<Album> albums = albumService.findAllByTitleContaining(album);
        Set<Track> tracksFromAlbums = new TreeSet<>();

        if (albums != null) {
            for (Album a : albums) {
                tracksFromAlbums.addAll(trackRepository.findAllByAlbum(a));
            }
        }
        if (albumsFromArtists != null) {
            for (Album a : albumsFromArtists) {
                setResultTracks.addAll(trackRepository.findAllByAlbum(a));
            }
        }
        if (tracksFromAlbums.size() != 0) {
            if (setResultTracks.size() != 0) {
                setResultTracks.retainAll(tracksFromAlbums);
            } else {
                setResultTracks.addAll(tracksFromAlbums);
            }
        }
        if (tracks != null && tracks.size() != 0) {
            if (setResultTracks.size() != 0) {
                setResultTracks.retainAll(tracks);
            } else {
                setResultTracks.addAll(tracks);
            }
        }
        resultTracks.addAll(setResultTracks);
        return resultTracks;
    }

    public List<Track> findAllByTitleContaining(String title) {
        if (title == "") {
            return null;
        }
        return trackRepository.findAllByTitleContaining(title);
    }

    public Track createWithFile(MultipartFile file, String title, Integer albumId) {
        Album album = albumService.findOne(albumId);
        if (album == null) {
            throw new RuntimeException("Can't find album in db");
        }
        Track track = create(new Track(
                title,
                null,
                album,
                null
        ));
        attributeService.generateAttributesForTrack(file, track.getId());
        return track;
    }
}
