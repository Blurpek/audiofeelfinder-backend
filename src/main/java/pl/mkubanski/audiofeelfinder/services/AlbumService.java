package pl.mkubanski.audiofeelfinder.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.mkubanski.audiofeelfinder.domains.Album;
import pl.mkubanski.audiofeelfinder.domains.Artist;
import pl.mkubanski.audiofeelfinder.repositories.AlbumRepository;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
@Transactional
public class AlbumService {
    @Autowired
    AlbumRepository albumRepository;

    @Autowired
    ArtistService artistService;

    public Album create(String title, Date releaseDate) {
        return albumRepository.saveAndFlush(new Album(title, releaseDate, null));
    }

    public Album createWithArtist(String title, Date releaseDate, Integer artistId) {
        Artist artist = artistService.findOne(artistId);
        if (artist == null) {
            throw new RuntimeException("Cannot find artist in db");
        }
        Album album = new Album(title, releaseDate, null);
        album.getArtists().add(artist);
        album = albumRepository.save(album);
//        artist.getAlbums().add(album);
//        album.getArtists().add(artist);
//        artistService.save(artist);
//        album = albumRepository.save(album);
        return album;
    }

    public ArrayList<Album> findAll() {
        return (ArrayList<Album>) albumRepository.findAll();
    }

    public Album findOne(Integer id) {
        return albumRepository.findOne(id);
    }

    public void delete(Integer id) {
        albumRepository.delete(id);
    }

    public List<Album> findAllByTitleContaining(String title) {
        if (title == "") {
            return null;
        }
        return albumRepository.findAllByTitleContaining(title);
    }

    public List<Album> listByArtist(Integer id) {
        Artist artist = artistService.findOne(id);
        if (artist == null) {
            throw new RuntimeException("Cannot find aritst in db");
        }
        return artist.getAlbums();
    }

    public List<Album> findAllByArtistsContaining(String artist) {
        return albumRepository.findAllByArtists(artistService.findAllByNameContaining(artist));
    }
}
