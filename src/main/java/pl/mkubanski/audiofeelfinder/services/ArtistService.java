package pl.mkubanski.audiofeelfinder.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.mkubanski.audiofeelfinder.domains.Album;
import pl.mkubanski.audiofeelfinder.domains.Artist;
import pl.mkubanski.audiofeelfinder.repositories.ArtistRepository;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class ArtistService {
    @Autowired
    ArtistRepository artistRepository;

    public List<Artist> findAllByNameContaining(String artist) {
        if (artist == "") {
            return null;
        }
        return artistRepository.findAllByNameContaining(artist);
    }

    public List<Artist> list() {
        return artistRepository.findAll();
    }

    public Artist findOne(Integer artistId) {
        return artistRepository.findOne(artistId);
    }

    public Artist create(String name) {
        return artistRepository.save(new Artist(name));
    }

    public Artist save(Artist artist) {
        return artistRepository.save(artist);
    }
}
