package pl.mkubanski.audiofeelfinder;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.data.jpa.convert.threeten.Jsr310JpaConverters;

//for jsr310 java 8 java.time.*
@EntityScan(
		basePackages = "pl.mkubanski.audiofeelfinder",
		basePackageClasses = {AudiofeelfinderApplication.class, Jsr310JpaConverters.class}
)
//@EnableJpaRepositories("pl.mkubanski.audiofeelfinder")
@SpringBootApplication
public class AudiofeelfinderApplication extends SpringBootServletInitializer {

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(AudiofeelfinderApplication.class);
	}

	public static void main(String[] args) {
		SpringApplication.run(AudiofeelfinderApplication.class, args);
	}
}
