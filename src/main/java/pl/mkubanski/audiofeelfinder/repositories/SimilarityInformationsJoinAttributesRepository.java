package pl.mkubanski.audiofeelfinder.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.mkubanski.audiofeelfinder.domains.SimilarityInformationsJoinAttributes;

@Repository
public interface SimilarityInformationsJoinAttributesRepository extends JpaRepository<SimilarityInformationsJoinAttributes, Integer> {
}
