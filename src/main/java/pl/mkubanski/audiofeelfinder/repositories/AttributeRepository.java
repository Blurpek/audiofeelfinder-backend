package pl.mkubanski.audiofeelfinder.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.mkubanski.audiofeelfinder.domains.Attribute;

@Repository
public interface AttributeRepository extends JpaRepository<Attribute, Integer> {
    Attribute findByName(String name);

}
