package pl.mkubanski.audiofeelfinder.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.mkubanski.audiofeelfinder.domains.Artist;

import java.util.List;

@Repository
public interface ArtistRepository extends JpaRepository<Artist, Integer> {
    @Override
    Artist findOne(Integer integer);

    List<Artist> findAllByNameContaining(String name);
}
