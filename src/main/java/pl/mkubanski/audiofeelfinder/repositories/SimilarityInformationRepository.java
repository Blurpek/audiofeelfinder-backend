package pl.mkubanski.audiofeelfinder.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.mkubanski.audiofeelfinder.domains.SimilarityComputationMethod;
import pl.mkubanski.audiofeelfinder.domains.SimilarityInformation;
import pl.mkubanski.audiofeelfinder.domains.Track;

@Repository
public interface SimilarityInformationRepository extends JpaRepository<SimilarityInformation, Integer> {
    public boolean existsByTrack1AndTrack2(Track track1, Track track2);
    public SimilarityInformation findByTrack1AndTrack2AndMethod(Track track1, Track track2, SimilarityComputationMethod method);
}
