package pl.mkubanski.audiofeelfinder.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.mkubanski.audiofeelfinder.domains.Album;
import pl.mkubanski.audiofeelfinder.domains.Artist;

import java.util.List;

@Repository
public interface AlbumRepository extends JpaRepository<Album, Integer> {
    @Override
    Album findOne(Integer integer);
    List<Album> findAllByTitleContaining(String title);
    List<Album> findAllByArtists(List<Artist> artists);
}
