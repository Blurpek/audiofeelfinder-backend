package pl.mkubanski.audiofeelfinder.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.mkubanski.audiofeelfinder.domains.SimilarityComputationMethod;

@Repository
public interface SimilarityComputationMethodRepository extends JpaRepository<SimilarityComputationMethod, Integer> {
}
