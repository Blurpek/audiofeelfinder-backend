package pl.mkubanski.audiofeelfinder.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.mkubanski.audiofeelfinder.domains.Album;
import pl.mkubanski.audiofeelfinder.domains.Track;

import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public interface TrackRepository extends JpaRepository<Track, Integer> {
    List<Track> findAllByTitleContaining(String title);
    List<Track> findAllByAlbum(Album album);
}
