package pl.mkubanski.audiofeelfinder.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.mkubanski.audiofeelfinder.domains.Attribute;
import pl.mkubanski.audiofeelfinder.domains.AttributeJoinTrack;
import pl.mkubanski.audiofeelfinder.domains.Track;

import java.util.List;

@Repository
public interface AttributeJoinTrackRepository extends JpaRepository<AttributeJoinTrack, Integer> {
    public boolean existsByTrack(Track track);
    public List<AttributeJoinTrack> findAllByTrack(Track track);
    public List<AttributeJoinTrack> findAllByAttributeAndTrack(Attribute attribute, Track track);
    public List<AttributeJoinTrack> findDistinctAttributeByTrack(Track track);
}
