package pl.mkubanski.audiofeelfinder.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.mkubanski.audiofeelfinder.domains.Attribute;
import pl.mkubanski.audiofeelfinder.domains.AttributeJoinSimilarityComputationMethod;
import pl.mkubanski.audiofeelfinder.domains.SimilarityComputationMethod;

@Repository
public interface AttributeJoinSimilarityComputationMethodRepository extends JpaRepository<AttributeJoinSimilarityComputationMethod, Integer> {
    public AttributeJoinSimilarityComputationMethod findByMethodAndAndAttribute(SimilarityComputationMethod method, Attribute attribute);
}
