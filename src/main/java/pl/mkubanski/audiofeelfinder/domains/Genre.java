package pl.mkubanski.audiofeelfinder.domains;

import lombok.Getter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Entity
@Table(name = "genres")
public class Genre implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "genre_id", nullable = false)
    @Getter
    private Integer id;

    @NotNull
    @Column(name = "genre_name", nullable = false)
    String name;

    public Genre() {}
}
