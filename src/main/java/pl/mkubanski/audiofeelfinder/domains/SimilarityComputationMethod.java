package pl.mkubanski.audiofeelfinder.domains;

import lombok.Getter;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "similarity_computation_methods")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "method_type")
public abstract class SimilarityComputationMethod implements SimilarityComputable, Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "similarity_computation_method_id", nullable = false)
    @Getter
    protected Integer id;

    @Getter
    @Column(name = "method_name")
    protected String name;

    @Getter
    @Column(name = "method_description")
    protected String description;
}
