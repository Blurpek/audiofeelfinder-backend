package pl.mkubanski.audiofeelfinder.domains;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Entity
@Table(name = "tracks_join_genres")
public class TrackJoinGenre implements Serializable {
    @Id
    @ManyToOne
    @JoinColumn(name = "track_id", nullable = false)
    @NotNull
    Track track;

    @Id
    @ManyToOne
    @JoinColumn(name = "genre_id", nullable = false)
    @NotNull
    Genre genre;
}
