package pl.mkubanski.audiofeelfinder.domains;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "artists")
public class Artist implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "artist_id", nullable = false)
    @Getter
    private Integer id;

    @Getter
    @NotNull
    @Column(name = "artist_name", nullable = false)
    String name;

    @JsonIgnore
    @Getter
    @ManyToMany(mappedBy = "artists")
    private List<Album> albums = new ArrayList<>();

    public Artist() {}

    public Artist(String name) {
        this.name = name;
    }
}