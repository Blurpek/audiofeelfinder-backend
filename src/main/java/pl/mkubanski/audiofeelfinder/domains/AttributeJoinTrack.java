package pl.mkubanski.audiofeelfinder.domains;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Entity
@Table(name = "attributes_join_tracks")
public class AttributeJoinTrack implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "attributes_join_tracks_id", nullable = false)
    @Getter
    @Setter
    private Integer id;

    @Getter
    @Setter
    @ManyToOne
    @JoinColumn(name = "attribute_id", nullable = false)
    @NotNull
    Attribute attribute;

    @Getter
    @Setter
    @ManyToOne
    @JoinColumn(name = "track_id", nullable = false)
    @NotNull
    Track track;

    @Getter
    @Column(name = "value_number", nullable = false)
    @NotNull
    Integer valueNumber;

    @Getter
    @Column(name = "value", nullable = false)
    @NotNull
    Double value;

    public AttributeJoinTrack() {}

    public AttributeJoinTrack(Attribute attribute, Track track, Integer valueNumber, Double value) {
        this.attribute = attribute;
        this.track = track;
        this.valueNumber = valueNumber;
        this.value = value;
    }
}
