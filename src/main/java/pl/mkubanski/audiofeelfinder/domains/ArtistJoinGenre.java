package pl.mkubanski.audiofeelfinder.domains;


import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Entity
@Table(name = "artists_join_genres")
public class ArtistJoinGenre implements Serializable {
    @Id
    @ManyToOne
    @JoinColumn(name = "artist_id", nullable = false)
    @NotNull
    Artist artist;

    @Id
    @ManyToOne
    @JoinColumn(name = "genre_id", nullable = false)
    @NotNull
    Genre genre;
}
