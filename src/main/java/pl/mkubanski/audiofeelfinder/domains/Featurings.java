package pl.mkubanski.audiofeelfinder.domains;

import lombok.Getter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Entity
@Table(name = "featurings")
public class Featurings implements Serializable {
    @Id
    @ManyToOne
    @NotNull
    @JoinColumn(name = "track_id", nullable = false)
    @Getter
    Track track;

    @Id
    @ManyToOne
    @JoinColumn(name = "artist_id", nullable = false)
    @NotNull
    @Getter
    Artist artist;
}
