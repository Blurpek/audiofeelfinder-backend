package pl.mkubanski.audiofeelfinder.domains;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.sql.Time;

@Entity
@Table(name = "tracks")
public class Track implements Serializable, Comparable<Track> {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "track_id", nullable = false)
    @Getter
    private Integer id;

    @Setter
    @Getter
    @NotNull
    @Column(name = "track_title", nullable = false)
    public String title;

    @Setter
    @Column(name = "duration")
    @Getter
    Time duration;

    @Getter
    @ManyToOne
    @JoinColumn(name = "album_id")
    Album album;

    @Setter
    @Column(name = "track_number")
    @Getter
    private Integer number;

    public Track(String title, Time duration, Album album, Integer number) {
        this.title = title;
        this.duration = duration;
        this.album = album;
        this.number = number;
    }

    public Track() {}

    @Override
    public int compareTo(Track track) {
        if (this.id == track.getId()) {
            return 0;
        } else {
            return 1;
        }
    }
}
