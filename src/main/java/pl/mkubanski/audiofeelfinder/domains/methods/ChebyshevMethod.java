package pl.mkubanski.audiofeelfinder.domains.methods;

import pl.mkubanski.audiofeelfinder.domains.Attribute;
import pl.mkubanski.audiofeelfinder.domains.AttributeJoinTrack;
import pl.mkubanski.audiofeelfinder.domains.SimilarityComputationMethod;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.util.List;

@Entity
@DiscriminatorValue(value = "chebyshev")
public class ChebyshevMethod extends SimilarityComputationMethod {
    @Override
    public Double compute(List<AttributeJoinTrack> trackFeatures,
                          List<AttributeJoinTrack> testingTrackFeatures) {

        //d(x,y) = max(|xi - yi|)
        Double localMax;
        Double globalMax = 0d;
        Double xi, yi;
        for (int i = 0; i < trackFeatures.size(); ++i) {
            xi = trackFeatures.get(i).getValue();
            yi = testingTrackFeatures.get(i).getValue();
            localMax = Math.max(xi, yi);
            if (localMax > globalMax) {
                globalMax = localMax;
            }
        }
        return globalMax;
    }

    public ChebyshevMethod() {}

    public ChebyshevMethod(String name, String description) {
        this.name = name;
        this.description = description;
    }
}
