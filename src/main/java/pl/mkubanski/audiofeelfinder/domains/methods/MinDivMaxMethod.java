package pl.mkubanski.audiofeelfinder.domains.methods;

import pl.mkubanski.audiofeelfinder.domains.Attribute;
import pl.mkubanski.audiofeelfinder.domains.AttributeJoinTrack;
import pl.mkubanski.audiofeelfinder.domains.SimilarityComputationMethod;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.util.List;

@Entity
@DiscriminatorValue(value = "jaccard")
public class MinDivMaxMethod extends SimilarityComputationMethod {
    @Override
    public Double compute(List<AttributeJoinTrack> trackFeatures,
                          List<AttributeJoinTrack> testingTrackFeatures) {

        //d(x,y) = E(min(xi,yi))/E(max(xi,yi))
        Double sumOfMin = 0d;
        Double sumOfMax = 0d;
        Double xi, yi;
        for (int i = 0; i < trackFeatures.size(); ++i) {
            xi = trackFeatures.get(i).getValue();
            yi = testingTrackFeatures.get(i).getValue();
            sumOfMin += Math.min(xi, yi);
            sumOfMax += Math.max(xi, yi);
        }
        return sumOfMax == 0d ? Double.MAX_VALUE : sumOfMin/sumOfMax;
    }

    public MinDivMaxMethod() {}

    public MinDivMaxMethod(String name, String description) {
        this.name = name;
        this.description = description;
    }
}
