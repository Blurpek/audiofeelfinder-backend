package pl.mkubanski.audiofeelfinder.domains.methods;

import pl.mkubanski.audiofeelfinder.domains.*;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.util.List;

@Entity
@DiscriminatorValue(value = "manhattan")
public class ManhattanMethod extends SimilarityComputationMethod {
    @Override
    public Double compute(List<AttributeJoinTrack> trackFeatures,
                          List<AttributeJoinTrack> testingTrackFeatures) {

        //Ei->n(a,b) abs(ai - bi)
        Double similarities = 0d;
        Double xi, yi;
        for (int i = 0; i < trackFeatures.size(); ++i) {
            xi = trackFeatures.get(i).getValue();
            yi = testingTrackFeatures.get(i).getValue();
            similarities += Math.abs(xi - yi);
        }
        return similarities;
    }

    public ManhattanMethod() {}

    public ManhattanMethod(String name, String description) {
        this.name = name;
        this.description = description;
    }
}
