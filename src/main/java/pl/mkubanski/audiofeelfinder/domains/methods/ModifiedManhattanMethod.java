package pl.mkubanski.audiofeelfinder.domains.methods;

import pl.mkubanski.audiofeelfinder.domains.Attribute;
import pl.mkubanski.audiofeelfinder.domains.AttributeJoinTrack;
import pl.mkubanski.audiofeelfinder.domains.SimilarityComputationMethod;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.util.List;

@Entity
@DiscriminatorValue(value = "modified_manhattan")
public class ModifiedManhattanMethod extends SimilarityComputationMethod {

    @Override
    public Double compute(List<AttributeJoinTrack> trackFeatures,
                          List<AttributeJoinTrack> testingTrackFeatures) {

        //Ei->n(a,b) abs(ai - bi) / abs(max(ai, bi))
        Double similarities = 0d;
        Double xi, yi, max;
        for (int i = 0; i < trackFeatures.size(); ++i) {
            xi = trackFeatures.get(i).getValue();
            yi = testingTrackFeatures.get(i).getValue();
            max = Math.abs(Math.max(xi, yi));
            similarities += Math.abs(xi - yi)/max == 0d ? 0.1d : max;
        }
        return similarities/trackFeatures.size();
    }

    public ModifiedManhattanMethod() {}

    public ModifiedManhattanMethod(String name, String description) {
        this.name = name;
        this.description = description;
    }
}
