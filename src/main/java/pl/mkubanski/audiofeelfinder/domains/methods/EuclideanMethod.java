package pl.mkubanski.audiofeelfinder.domains.methods;

import pl.mkubanski.audiofeelfinder.domains.Attribute;
import pl.mkubanski.audiofeelfinder.domains.AttributeJoinTrack;
import pl.mkubanski.audiofeelfinder.domains.SimilarityComputationMethod;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.util.List;

@Entity
@DiscriminatorValue(value = "euclidean")
public class EuclideanMethod extends SimilarityComputationMethod {

    @Override
    public Double compute(List<AttributeJoinTrack> trackFeatures,
                          List<AttributeJoinTrack> testingTrackFeatures) {

        //d(x,y) = sqrt(Ei->n(pow(yi-xi))) 
        Double similarities = 0d;
        Double xi, yi;
        for (int i = 0; i < trackFeatures.size(); ++i) {
            xi = trackFeatures.get(i).getValue();
            yi = testingTrackFeatures.get(i).getValue();
            similarities += Math.pow(yi-xi, 2);
        }
        return Math.sqrt(similarities);
    }

    public EuclideanMethod() {}

    public EuclideanMethod(String name, String description) {
        this.name = name;
        this.description = description;
    }
}
