package pl.mkubanski.audiofeelfinder.domains.methods;

import pl.mkubanski.audiofeelfinder.domains.Attribute;
import pl.mkubanski.audiofeelfinder.domains.AttributeJoinTrack;
import pl.mkubanski.audiofeelfinder.domains.SimilarityComputationMethod;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.util.List;

@Entity
@DiscriminatorValue(value = "complete_metric")
public class CompleteMetricMethod extends SimilarityComputationMethod {
    @Override
    public Double compute(List<AttributeJoinTrack> trackFeatures,
                          List<AttributeJoinTrack> testingTrackFeatures) {

        //d(x,y) = |log(y-x)|
        Double similarities = 0d;
        Double xi, yi;
        for (int i = 0; i < trackFeatures.size(); ++i) {
            xi = trackFeatures.get(i).getValue();
            yi = testingTrackFeatures.get(i).getValue();
            similarities += Math.log(Math.abs(yi-xi));
        }
        return similarities;
    }

    public CompleteMetricMethod() {}

    public CompleteMetricMethod(String name, String description) {
        this.name = name;
        this.description = description;
    }
}
