package pl.mkubanski.audiofeelfinder.domains.methods;

import pl.mkubanski.audiofeelfinder.domains.Attribute;
import pl.mkubanski.audiofeelfinder.domains.AttributeJoinTrack;
import pl.mkubanski.audiofeelfinder.domains.SimilarityComputationMethod;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.util.List;

@Entity
@DiscriminatorValue(value = "nn_brute_force")
public class NNBruteForceMethod extends SimilarityComputationMethod {
    @Override
    public Double compute(List<AttributeJoinTrack> trackFeatures,
                          List<AttributeJoinTrack> testingTrackFeatures) {

        //NN = nearest neighbour
        Double minDistance = Double.MAX_VALUE;
        Double newDistance = 0d;
        for (int i = 0; i < trackFeatures.size(); ++i) {
            for (int j = 0; j < testingTrackFeatures.size(); ++j) {
                newDistance = computeDistance(
                    trackFeatures.get(i).getValue(),
                    testingTrackFeatures.get(j).getValue()
                );
                if (newDistance < minDistance) {
                    minDistance = newDistance;
                }
            }
        }
        return minDistance;
    }

    private Double computeDistance(Double x, Double y) {
        return Math.abs(x - y);
    }

    public NNBruteForceMethod() {}

    public NNBruteForceMethod(String name, String description) {
        this.name = name;
        this.description = description;
    }
}
