package pl.mkubanski.audiofeelfinder.domains;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Entity
@Table(name = "similarity_informations_join_attributes")
public class SimilarityInformationsJoinAttributes implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "similarity_informations_join_attributes_id", nullable = false)
    @Getter
    @Setter
    private Integer id;

    @Getter @Setter
    @ManyToOne
    @JoinColumn(name = "attribute_id", nullable = false)
    @NotNull
    Attribute attribute;

    @Setter
    @NotNull
    @ManyToOne
    @JoinColumn(name = "similarity_information_id", nullable = false)
    SimilarityInformation similarityInformation;

    @Getter @Setter
    @Column(name = "similarity")
    Double similarity;

    public SimilarityInformationsJoinAttributes() {}

    public SimilarityInformationsJoinAttributes(Attribute attribute, SimilarityInformation similarityInformation, Double similarity) {
        this.attribute = attribute;
        this.similarityInformation = similarityInformation;
        this.similarity = similarity;
    }
}
