package pl.mkubanski.audiofeelfinder.domains;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "attribute_join_similarity_computation_method")
public class AttributeJoinSimilarityComputationMethod {
    @Getter
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "attribute_join_similarity_computation_method_id")
    private Integer id;

    @NotNull
    @Getter
    @ManyToOne
    @JoinColumn(name = "attribute_id")
    private Attribute attribute;

    @NotNull
    @Getter
    @ManyToOne
    @JoinColumn(name = "method_id")
    private SimilarityComputationMethod method;

    @Setter
    @Getter
    @Column(name = "max")
    private Double max;

    @Setter
    @Getter
    @Column(name = "min")
    private Double min;

    public AttributeJoinSimilarityComputationMethod() {}

    public AttributeJoinSimilarityComputationMethod(Attribute attribute, SimilarityComputationMethod method, Double max, Double min) {
        this.attribute = attribute;
        this.method = method;
        this.max = max;
        this.min = min;
    }
}
