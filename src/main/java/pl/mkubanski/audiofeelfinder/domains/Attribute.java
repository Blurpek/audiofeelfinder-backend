package pl.mkubanski.audiofeelfinder.domains;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Entity
@Table(name = "attributes")
public class Attribute implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "attribute_id", nullable = false)
    @Getter
    private Integer id;

    @Getter
    @NotNull
    @Column(name = "attribute_name", nullable = false)
    private String name;

    @Getter
    @Column(name = "attribute_description")
    private String description;

    public Attribute() {}

    public Attribute(String name) {
        this.name = name;
    }

    public Attribute(String name, String description) {
        this.name = name;
        this.description = description;
    }
}
