package pl.mkubanski.audiofeelfinder.domains;

import java.util.List;

public interface SimilarityComputable {
    Double compute(List<AttributeJoinTrack> trackFeatures,
                   List<AttributeJoinTrack> testingTrackFeatures);
}
