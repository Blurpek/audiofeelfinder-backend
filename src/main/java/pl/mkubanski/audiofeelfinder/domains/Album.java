package pl.mkubanski.audiofeelfinder.domains;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.sql.Blob;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "albums")
public class Album implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "album_id", nullable = false)
    @Getter
    @Setter
    private Integer id;

    @Setter
    @Getter
    @NotNull
    @Column(name = "album_title", nullable = false)
    private String title;

    @Setter
    @Getter
    @Column(name = "release_date")
    private Date releaseDate;

    @Setter
    @Getter
    @Column(name = "cover", columnDefinition = "BLOB")
    @Lob
    private Blob cover;

    @JsonIgnore
    @Getter
    @ManyToMany
    @JoinTable(
            name = "artists_join_albums ",
            joinColumns = @JoinColumn(name = "album_id"),
            inverseJoinColumns = @JoinColumn(name = "artist_id")
    )
    private List<Artist> artists = new ArrayList<>();

    public Album() {}

    public Album(String title, Date releaseDate, Blob cover) {
        this.title = title;
        this.releaseDate = releaseDate;
        this.cover = cover;
    }
}
