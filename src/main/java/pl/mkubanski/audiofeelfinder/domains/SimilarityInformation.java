package pl.mkubanski.audiofeelfinder.domains;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "similarity_informations")
public class SimilarityInformation implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "similarity_information_id", nullable = false)
    @Getter
    private Integer id;

    @Getter
    @ManyToOne
    @JoinColumn(name = "track1_id", nullable = false)
    @NotNull
    Track track1;

    @Getter
    @ManyToOne
    @JoinColumn(name = "track2_id", nullable = false)
    @NotNull
    Track track2;

    @Getter
    @OneToMany(mappedBy = "similarityInformation")
    List<SimilarityInformationsJoinAttributes> similarities = new ArrayList<SimilarityInformationsJoinAttributes>();

    @Getter
    @ManyToOne
    @JoinColumn(name = "method_id", nullable = false)
    @NotNull
    SimilarityComputationMethod method;

    public SimilarityInformation() {}

    public SimilarityInformation(Track track1,
                                 Track track2,
                                 List<SimilarityInformationsJoinAttributes> similarities,
                                 SimilarityComputationMethod method) {
        this.track1 = track1;
        this.track2 = track2;
        this.similarities = similarities;
        this.method = method;
    }

    public SimilarityInformation(Track track1,
                                 Track track2,
                                 SimilarityComputationMethod method) {
        this.track1 = track1;
        this.track2 = track2;
        this.method = method;
    }
}
